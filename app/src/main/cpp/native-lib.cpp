#include <jni.h>
#include <android/log.h>
#include <cpu-features.h>

#include <time.h>
#include <cstdlib>
#include <pthread.h>

using namespace std;

//
// ORIGINAL CODE HERE

/*
extern "C"
void
Java_com_speedtest_speedtest_speedtest_MainActivity_foo(
        JNIEnv *env,
        jobject) {


    const static int csCount1=4096, csCount2=15, csCount3=27500, csCount4=39000, csCount5=33500;
    int aIndex1[csCount1][csCount2], aIndex2[csCount3];
    float aValue1[csCount5+csCount4];
    int aIter1, aIter2, aIter3;
    double aSum=0.f;
    //setup:
    ::srand(1111);
    for (aIter1=0; aIter1<csCount1; ++aIter1)
        for (aIter2=0; aIter2<csCount2; ++aIter2)
            aIndex1[aIter1][aIter2]=rand()%csCount3;
    for (aIter1=0; aIter1<csCount3; ++aIter1)
        aIndex2[aIter1]=rand()%csCount4;
    for (aIter1=0; aIter1<csCount5+csCount4; ++aIter1)
        aValue1[aIter1]=float(rand())/RAND_MAX;
    //test:
    time_t aTime1=clock();
    for (aIter3=0; aIter3<csCount5; ++aIter3)
        for (aIter1=0; aIter1<csCount1; ++aIter1)
        {
            aIter2=aIndex2[(aIter3+aIter1)%csCount3]%csCount2;
            aSum+=aValue1[aIter3+aIndex2[aIndex1[aIter1][aIter2]]];
        }
    time_t aTime2=clock();

    __android_log_print(ANDROID_LOG_DEBUG, "SpeedTest", "time= %f sum= %f\n", float(aTime2-aTime1)/CLOCKS_PER_SEC, aSum);
}*/



//
// PARALLELIZED VERSION HERE
//

// Globals, for thread access
const static int MAX_CPU_NUM = 12;
const static int csCount1=4096, csCount2=15, csCount3=27500, csCount4=39000, csCount5=33500;
int aIndex1[csCount1][csCount2], aIndex2[csCount3];
float aValue1[csCount5+csCount4];
double aSumArray[MAX_CPU_NUM];
int numThreads;


void *thread_task(void *arg) {


    int tid = *((int *) arg);
    int tIter2;
    double tSum = 0.f;

    __android_log_print(ANDROID_LOG_DEBUG, "SpeedTest", "thread %d starting", tid);

    // Same loop as original code, except that each thread iterates from an index equal to tid,
    // and jumps by numThreads
    for (int tIter3 = tid; tIter3 < csCount5; tIter3 += numThreads) {
        for (int tIter1 = 0; tIter1 < csCount1; ++tIter1) {
            tIter2 = aIndex2[(tIter3 + tIter1) % csCount3] % csCount2;
            tSum += aValue1[tIter3 + aIndex2[aIndex1[tIter1][tIter2]]];
        }
    }

    aSumArray[tid] = tSum;

    __android_log_print(ANDROID_LOG_DEBUG, "SpeedTest", "thread %d finished, sum %f", tid, tSum);


    pthread_exit(0);
}


extern "C"
void
Java_com_speedtest_speedtest_speedtest_MainActivity_foo(
        JNIEnv *env,
        jobject) {

    int aIter1, aIter2, aIter3;
    double aSum=0.f;
    //setup:
    ::srand(1111);
    for (aIter1=0; aIter1<csCount1; ++aIter1)
        for (aIter2=0; aIter2<csCount2; ++aIter2)
            aIndex1[aIter1][aIter2]=rand()%csCount3;
    for (aIter1=0; aIter1<csCount3; ++aIter1)
        aIndex2[aIter1]=rand()%csCount4;
    for (aIter1=0; aIter1<csCount5+csCount4; ++aIter1)
        aValue1[aIter1]=float(rand())/RAND_MAX;


    //test:

    // clock() DOES NOT MEASURE ACTUAL EXECUTION TIME WHEN MULTIPLE CORES ARE BEING USED!
    //time_t aTime1=clock();

    const static int NANOSECS = 1000000000;
    struct timespec tSpec;
    clock_gettime(CLOCK_MONOTONIC, &tSpec);
    float aTime1 = tSpec.tv_sec + tSpec.tv_nsec / (float)NANOSECS;

    //
    // Parallelized version
    //

    // Ask Android for CPU core count
    numThreads = android_getCpuCount();
    if (numThreads > MAX_CPU_NUM)
        numThreads = MAX_CPU_NUM;

    __android_log_print(ANDROID_LOG_DEBUG, "SpeedTest", "num of CPUs: %d\n", numThreads);


    // Spawn threads with tid argument
    pthread_t threads[numThreads];
    int thread_args[numThreads];
    int res;

    __android_log_print(ANDROID_LOG_DEBUG, "SpeedTest", "starting threads...\n", numThreads);



    for (int i = 0; i < numThreads; ++i)
    {
        aSumArray[i] = 0.f;
        thread_args[i] = i;
        res = pthread_create(&threads[i], NULL, thread_task, (void*) &thread_args[i]);

        if (res)
        {
            __android_log_print(ANDROID_LOG_DEBUG, "SpeedTest", "pthreads error");
            exit(-1);
        }
    }

    // Join threads and sum the result
    for (int i = 0; i < numThreads; ++i)
    {
        res = pthread_join(threads[i], NULL);

        if (res)
        {
            __android_log_print(ANDROID_LOG_DEBUG, "SpeedTest", "pthreads error");
            exit(-1);
        }

        aSum += aSumArray[i];
    }

    // clock() DOES NOT MEASURE ACTUAL EXECUTION TIME WHEN MULTIPLE CORES ARE BEING USED!
  //  time_t aTime2=clock();
    clock_gettime(CLOCK_MONOTONIC, &tSpec);
    float aTime2 = tSpec.tv_sec + tSpec.tv_nsec / (float)NANOSECS;

    __android_log_print(ANDROID_LOG_DEBUG, "SpeedTest", "time= %f sum= %f\n", aTime2-aTime1, aSum);
}
